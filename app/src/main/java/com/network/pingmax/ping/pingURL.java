/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.pingmax.ping;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 *
 * @author talal
 */
public class pingURL implements Runnable {
    private static final String LOG_TAG = pingURL.class.getSimpleName();
    private URL url;
    private int numberOfPings;
    private int success;
    private long interval;
    private long minRTT;
    private long maxRTT;
    private long totalRTT;
    private String log;
    private boolean error;

    public pingURL(String url, int count, long interval) {
        try {
            if (!url.contains("http://")) {
                url = "http://" + url;
            }
            this.url = new URL(url);
        } catch (MalformedURLException e) {
//            result = "Red";
            System.out.println(LOG_TAG+" Error: " + e.getMessage() + "\n Stack: " + e.getStackTrace());
        }

        this.numberOfPings = count;
        this.interval = interval;
        this.minRTT = 0;
        this.maxRTT = 0;
        this.totalRTT = 0;
        this.log = "Log:\n";
        this.error = false;
    }

    public void run(){

        for (int i = numberOfPings; i > 0; i--) {
            long start = 0, stop = 0;
            long timeToRespond = -1;
            try {
                System.out.println(LOG_TAG+" Pinging ");
                start = System.currentTimeMillis();
                HttpURLConnection connection = (HttpURLConnection) this.url
                        .openConnection();

                connection.setRequestMethod("GET");
                connection.connect();
                stop = System.currentTimeMillis();
                timeToRespond = stop - start;

                System.out.println(LOG_TAG+" Time: " + timeToRespond);

                if (timeToRespond > -1) {
                    processRTT(timeToRespond);
                }

                //Logging
                StringBuilder currentLog = new StringBuilder();
                currentLog.append("RTT=" + timeToRespond);
                Map<String, List<String>> map = connection.getHeaderFields();
                for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                    currentLog.append(entry.getKey() + " : " + entry.getValue());
                }
                this.appendLog(currentLog.toString());



            } catch (Exception e) {
                System.out.println(LOG_TAG+" Error: " + e.getMessage() + "\n Stack: " + e.getStackTrace());
                this.error = true;
//                e.printStackTrace();
            }

        }
    }

    public long getMinRTT() {
        return minRTT;
    }

    public void setMinRTT(long minRTT) {
        this.minRTT = minRTT;
    }

    public long getMaxRTT() {
        return maxRTT;
    }

    public void setMaxRTT(long maxRTT) {
        this.maxRTT = maxRTT;
    }

    public long getTotalRTT() {
        return totalRTT;
    }

    public void setTotalRTT(long totalRTT) {
        this.totalRTT = totalRTT;
    }

    public long getAvgRTT() {
        if(success>0) {
            return totalRTT / success;
        }else{
            return -1;
        }
    }

    public String getLog() {
        return log;
    }

    public void appendLog(String log) {
        this.log += "\n" + log;
    }

    @Override
    public String toString() {
//        return "";
        if(error){
            return url.toString() + " is an invalid URL";
        }
        return url.toString() + " with\n RTT:Avg="+this.getAvgRTT()+", min="+this.minRTT+", max="+this.maxRTT+"";
    }
    
    public void processRTT(long rtt) {
        this.totalRTT += rtt;
        this.success++;
//        System.out.println("RTT: " + rtt + " min: " + this.minRTT + " max:" + this.maxRTT);
        if (rtt < this.minRTT || this.minRTT == 0) {
            setMinRTT(rtt);
        } 
        if (rtt > this.maxRTT) {
            setMaxRTT(rtt);
        }
    }
}
