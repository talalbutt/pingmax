/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.pingmax.ping;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;
import java.util.Date;

/**
 *
 * @author talal
 */
public class Ping {

    public static void main(String args[]) throws Exception {

//        String[] hostList = {"http://siasat.pk", "yahoo.com",
//            "ebay.com", "google.com",
//            "example.co", "paypal.com",
//            "bing.com", "techcrunch.com",
//            "mashable.com", "thenextweb.com",
//            "wordpress.com", "wordpress.org",
//            "example.com", "sjsu.edu",
//            "ebay.co.uk", "google.co.uk",
//            "wikipedia.org",
//            "en.wikipedia.org/wiki/Main_Page"};
        String[] hostList = {"http://siasat.pk", "yahoo.com"};

        PingCollection pingsToURLs = new PingCollection(hostList, 2);
        pingsToURLs.startPingComparison();
        System.out.println(pingsToURLs.getLogReport());

//        pingURL[] pingList = new pingURL[hostList.length];

//        for (int i = 0; i < hostList.length; i++) {
//            String url = hostList[i];
//            System.out.println("Pinging HTTP Server");
//            pingURL pingNext = new pingURL(url, 2, 0);
//            pingNext.startPing();


//        System.out.println("Pinging at Layer 3 using JAVA based inetAddress.isReachable()");
//            status = pingAtLayerThreeJAVA(url);
//        System.out.println("Pinging at Layer 3 using OS based ping command");
//            status = pingAtLayerThreeOS(url);
//        System.out.println("Pinging at Layer 4 using socket");
//        status = pingUsingSocket(url,80);
//
//            System.out.println(url + "-Avg RTT:" + pingNext.getAvgRTT());
//            System.out.println("\t\tMin RTT:" + pingNext.getMinRTT());
//            System.out.println("\t\tMax RTT:" + pingNext.getMaxRTT());
//            System.out.println("\n\tLog:" + pingNext.getLog());
//        }

    }

    public static long pingURL(String url) throws IOException {

        String result = "";
        long start = 0, stop = 0;

        try {
            if (!url.contains("http://")) {
                url = "http://" + url;
            }

            URL siteURL = new URL(url);
            start = System.currentTimeMillis();
            HttpURLConnection connection = (HttpURLConnection) siteURL
                    .openConnection();

            connection.setRequestMethod("GET");
            connection.connect();
            stop = System.currentTimeMillis();

//            Map<String, List<String>> map = connection.getHeaderFields();
//            System.out.println("Printing All Response Header for URL: "
//                    + siteURL.toString() + "\n");
//
//            for (Map.Entry<String, List<String>> entry : map.entrySet()) {
//                System.out.println(entry.getKey() + " : " + entry.getValue());
//            }
//            int code = connection.getResponseCode();
//            if (code == 200) {
//                result = "Green";
//            }

        } catch (Exception e) {
//            result = "Red";
            System.out.println("Error: " + e.getMessage() + "\n Stack: " + e.getStackTrace());
        }

        return (stop - start);

    }

    /**
     * Connect using layer3
     *
     * @param hostAddress
     * @return delay if the specified host responded, -1 if failed
     */
    static long pingAtLayerThreeJAVA(String hostAddress) {
        InetAddress inetAddress = null;
        long start, stop;

        hostAddress = hostAddress.replace("http://", "");
        if (hostAddress.contains("/")) {
            System.out.println("At layer 3 can't do that");
            return -1;
        }

        try {
            inetAddress = InetAddress.getByName(hostAddress);
        } catch (UnknownHostException e) {
            System.out.println("Problem, unknown host:");
            e.printStackTrace();
        }

        try {
            start = System.currentTimeMillis();
            if (inetAddress.isReachable(5000)) {
                stop = System.currentTimeMillis();
                return (stop - start);
            }

        } catch (IOException e1) {
            System.out.println("Problem, a network error has occurred:");
            e1.printStackTrace();
        } catch (IllegalArgumentException e1) {
            System.out.println("Problem, timeout was invalid:");
            e1.printStackTrace();
        }

        return -1; // to indicate failure

    }

    static long pingAtLayerThreeOS(String hostAddress) {
        InetAddress inetAddress = null;
        long start, stop;

        hostAddress = hostAddress.replace("http://", "");
        if (hostAddress.contains("/")) {
            System.out.println("At layer 3 can't do that");
            return -1;
        }

        try {
            String cmd = "";
            if (System.getProperty("os.name").startsWith("Windows")) {
                // For Windows
                cmd = "ping -n 1 " + hostAddress;
            } else {
                // For Linux and OSX
                cmd = "ping -c 1 " + hostAddress;
            }

            start = System.currentTimeMillis();
            Process myProcess = Runtime.getRuntime().exec(cmd);
            myProcess.waitFor();
            stop = System.currentTimeMillis();

            if (myProcess.exitValue() == 0) {
                return stop - start;
            } else {

                return -1;
            }

        } catch (Exception e) {

            e.printStackTrace();
            return -1;
        }

    }

    /**
     * Connect using layer4 (sockets)
     *
     * @param
     * @return delay if the specified host responded, -1 if failed
     */
    static long pingUsingSocket(String hostAddress, int port) {
        InetAddress inetAddress = null;
        InetSocketAddress socketAddress = null;
        SocketChannel sc = null;
        long timeToRespond = -1;
        long start = 0, stop = 0;

        hostAddress = hostAddress.replace("http://", "");
        if (hostAddress.contains("/")) {
            System.out.println("Raw Sockets: Can't address resource");
            return -1;
        }

        try {
            inetAddress = InetAddress.getByName(hostAddress);
        } catch (UnknownHostException e) {
            System.out.println("Problem, unknown host:");
            e.printStackTrace();
        }

        try {
            socketAddress = new InetSocketAddress(inetAddress, port);
        } catch (IllegalArgumentException e) {
            System.out.println("Problem, port may be invalid:");
            e.printStackTrace();
        }

        // Open the channel, set it to non-blocking, initiate connect
        try {
            sc = SocketChannel.open();
            sc.configureBlocking(true);
            start = System.currentTimeMillis();
            if (sc.connect(socketAddress)) {
                stop = System.currentTimeMillis();
                timeToRespond = stop - start;
            }
        } catch (IOException e) {
            System.out.println("Problem, connection could not be made:");
            e.printStackTrace();
        }

        try {
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return timeToRespond;
    }
}