/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.pingmax.ping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author talal
 */
public class PingCollection {

    private static final String LOG_TAG = PingCollection.class.getSimpleName();
    private String hostlist[];
    private int numberofHosts;
    private ArrayList<pingURL> pingURLs;
    private int numberOfPings;
    private pingURL minAvgRTTHost;
    private pingURL maxAvgRTTHost;
    private String logReport;

    public PingCollection(String[] hostlist, int count) {
        this.hostlist = hostlist;
        this.numberofHosts = hostlist.length;
        this.pingURLs = new ArrayList<pingURL>();
        this.numberOfPings = count;
        for (String url : hostlist) {
            pingURL pingNext = new pingURL(url, this.numberOfPings, 0);
            pingURLs.add(pingNext);
        }
        this.logReport = "";
        minAvgRTTHost = null;
        maxAvgRTTHost = null;
    }

    public String startPingComparison(){

        //limit the number of actual threads
//        int poolSize = 10;
//        ExecutorService service = Executors.newFixedThreadPool(poolSize);
        try{
        ExecutorService service = Executors.newCachedThreadPool();
        List<Future<Runnable>> futures = new ArrayList<Future<Runnable>>();
        for (Iterator<pingURL> i = pingURLs.iterator(); i.hasNext();) {
            pingURL nextPing = (pingURL) i.next();
            Future f = service.submit(nextPing);
            futures.add(f);
        }

        // wait for all tasks to complete before continuing
        for (Future<Runnable> f : futures) {
            f.get();
        }

        //shut down the executor service so that this thread can exit
        service.shutdownNow();
        for (Iterator<pingURL> i = pingURLs.iterator(); i.hasNext();) {
            pingURL nextPing = (pingURL) i.next();
            if (nextPing.getAvgRTT() > -1) {
                this.ProcessPingResult(nextPing);
            }
            appendLog(nextPing.toString() + "\n");
        }
        }catch(ExecutionException ex){
            ex.getStackTrace();
        }catch(InterruptedException ex){
            ex.getStackTrace();            
        }
        return this.getLogReport();
    }

//    public String startPingComparison() throws IOException {
//        for (Iterator<pingURL> i = pingURLs.iterator(); i.hasNext();) {
//            pingURL nextPing = (pingURL) i.next();
//            nextPing.startPing();
//            appendLog(nextPing.toString()+"\n");
//            this.ProcessPingResult(nextPing);
//        }
//        return this.getLogReport();
//    }
    public pingURL getMinAvgRTTHost() {
        return minAvgRTTHost;
    }

    public void setMinAvgRTTHost(pingURL minAvgRTTHost) {
        this.minAvgRTTHost = minAvgRTTHost;
    }

    public pingURL getMaxAvgRTTHost() {
        return maxAvgRTTHost;
    }

    public void setMaxAvgRTTHost(pingURL maxAvgRTTHost) {
        this.maxAvgRTTHost = maxAvgRTTHost;
    }

    public void ProcessPingResult(pingURL newPingHost) {
        if (minAvgRTTHost == null && maxAvgRTTHost == null) {
            minAvgRTTHost = newPingHost;
            maxAvgRTTHost = newPingHost;
            return;
        } else if (minAvgRTTHost == null) {
            minAvgRTTHost = newPingHost;
            return;
        } else if (maxAvgRTTHost == null) {
            maxAvgRTTHost = newPingHost;
            return;
        }


        if (newPingHost.getAvgRTT() < this.minAvgRTTHost.getAvgRTT()) {
            setMinAvgRTTHost(newPingHost);
            return;
        }
        if (newPingHost.getAvgRTT() > this.maxAvgRTTHost.getAvgRTT()) {
            setMaxAvgRTTHost(newPingHost);
            return;
        }
    }

    public String[] getHostlist() {
        return hostlist;
    }

    public void setHostlist(String[] hostlist) {
        this.hostlist = hostlist;
    }

    public int getNumberofHosts() {
        return numberofHosts;
    }

    public void setNumberofHosts(int numberofHosts) {
        this.numberofHosts = numberofHosts;
    }

    public String getLogReport() {
        StringBuilder log = new StringBuilder();
        log.append("----Log Report [All times are in milliseconds]---\n");
        log.append("BEST RTT host:  ");
        log.append(minAvgRTTHost.toString() + "\n");
        log.append("WORST RTT host: ");
        log.append(maxAvgRTTHost.toString());
        log.append("\n\n\n--- Detail of all--- \n");
        log.append(logReport);
        return log.toString();
    }

    public void appendLog(String log) {
        this.logReport += log;
    }
}
