package com.network.pingmax;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.network.pingmax.ping.PingCollection;

import java.io.IOException;

/**
 * Created by talal on 01/02/2015.
 */
public class PingCollectionService extends Service {
    private static final String LOG_TAG = PingCollectionService.class.getSimpleName();
    public static final String
            ACTION_LOG_REPORT_BROADCAST = PingCollectionService.class.getName() + "logReportBroadcast",
            LOG_REPORT_LONG = "log_report_long";


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "onCreated Ping service");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartcommand SA service");
        if (intent != null) {
            int count = intent.getIntExtra("count", 1);
            String hostListItems = intent.getStringExtra("hostList");

            String[] hostList = hostListItems.split(",");
            doPings(hostList, count);
        }
        return START_REDELIVER_INTENT;
    }

    public void doPings(final String[] hostList, final int count) {
        Runnable runnable = new Runnable() {
            private long startTime = System.currentTimeMillis();

            public void run() {
                Log.d(LOG_TAG, "Work for " + hostList);
                StringBuilder sReport = new StringBuilder();
                sReport.append("Pinging " + hostList.length + " hosts: " + count + " times each\n");
                for (String s : hostList) {
                    sReport.append(s + "\n");
                    Log.v(LOG_TAG, "Host:" + s);
                }
                sendBroadcastMessage(sReport.toString());

                sReport.setLength(0);
                PingCollection pingsToURLs = new PingCollection(hostList, count);
                sReport.append("Collection Pinging " + hostList.length + " hosts: " + count + " times each\n");
                for (String s : pingsToURLs.getHostlist()) {
                    sReport.append(s + "\n");
                    Log.v(LOG_TAG, "Host:" + s);
                }
                sendBroadcastMessage(sReport.toString());
                pingsToURLs.startPingComparison();
                sendBroadcastMessage(pingsToURLs.getLogReport());
            }

        };
        new Thread(runnable).start();
    }


    private void sendBroadcastMessage(String logReport) {
        if (logReport != null) {
            Intent intent = new Intent(ACTION_LOG_REPORT_BROADCAST);
            intent.putExtra(LOG_REPORT_LONG, logReport);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
