package com.network.pingmax;

import java.net.InetAddress;

/**
 * Created by talal on 01/02/2015.
 */

public class PingerItem {
    String hostname;
    InetAddress ipAddress;
    long result_80; // connect 80
    long result_av; // isAvailable

    public PingerItem(String hostname) {
        this.hostname = hostname;
        this.ipAddress = null;
    }

    public PingerItem(String hostname, InetAddress ia, long result_80, long result_av) {
        this.hostname = hostname;
        this.ipAddress = ia;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public InetAddress getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(InetAddress ipAddress) {
        this.ipAddress = ipAddress;
    }
}
