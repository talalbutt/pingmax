package com.network.pingmax;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.network.pingmax.PingerItem;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import android.os.Handler;
import android.content.DialogInterface;
import android.widget.Toast;

/**
 * Created by talal on 01/02/2015.
 */
public class PingMax  extends Activity {
    private static final String LOG_TAG = PingMax.class.getSimpleName();
    private TextView myTextView = null;
    private EditText myEditText = null;
    private EditText repeatPings = null;
    private Button doAllButton = null;
    private final static String SAVEFILE="hosts";
    Activity content=null;
    final static int MAXTIME = 100000;
    final static int TIMEOUT = 3000;
    final static long PERIOD = 5000;
    final static long UPDATE = 1000;
    final static int MAXHOSTS = 15;
    int m_position = 0;
    final ArrayList<PingerItem> items = new ArrayList<PingerItem>();
    PingItemAdapter pingItemsAdapter =null;
    Thread m_background=null;
    boolean isRunning=false;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String localIp = getLocalIpAddress();
            if (localIp == null) localIp = "unknown";
            myTextView.setText("local IP : " + localIp);
            pingItemsAdapter.notifyDataSetChanged();
        }
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        content=this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        myEditText = (EditText)findViewById(R.id.myEditText);
        myTextView = (TextView)findViewById(R.id.myTextView);
        repeatPings = (EditText)findViewById(R.id.countText);
        doAllButton = (Button)findViewById(R.id.doAllButton);

        final ListView myListView = (ListView)findViewById(R.id.myListView);
        final Button myButton = (Button) findViewById(R.id.myButton);

        String localIp = getLocalIpAddress();
        if (localIp == null) localIp = "unknown";
        myTextView.setText("local IP : " + localIp);

        pingItemsAdapter = new PingItemAdapter(this, items);
        myListView.setAdapter(pingItemsAdapter);
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick(AdapterView av, View v, int position, long arg) {

                m_position = position;

                new AlertDialog.Builder(content)
                        .setTitle("Confirm")
                        .setMessage("Delete '" + items.get(m_position).hostname + "'")
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                items.remove(m_position);
                                pingItemsAdapter.notifyDataSetChanged();
                                if(items.size()<MAXHOSTS) {
                                    myEditText.setText("");
                                    myEditText.setEnabled(true);
                                }
                                saveItems();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //
                            }
                        })
                        .setCancelable(true)
                        .show();

            }
        });

//        myEditText.setOnKeyListener(new View.OnKeyListener(){
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if(event.getAction()==KeyEvent.ACTION_DOWN)
//                {
//                    if(keyCode==KeyEvent.KEYCODE_DPAD_CENTER || keyCode==KeyEvent.KEYCODE_ENTER) {
//                        String hostname = myEditText.getText().toString();
//                        hostname = hostname.replace('*', '.');
//                        boolean result = AddHostName(hostname);
//                        saveItems();
//                        return result;
//                    }
//                }
//                return false;
//            }
//        });

        myButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String hostname = myEditText.getText().toString().replaceAll("\\s","");
                if(!TextUtils.isEmpty(hostname.trim())) {
                    hostname = hostname.replace('*', '.');
                    AddHostName(hostname);
                    saveItems();
                }else{
                    Toast.makeText(PingMax.this, "Empty URL will not work", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        });

        doAllButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (!items.isEmpty()) {
                    if(isNetworkAvailable()) {
                        Log.v("PingMax:", "Doing Ping for all");

                        Intent myIntent = new Intent(PingMax.this, PingCollectionReportActivity.class);

                        Log.v(LOG_TAG, "Repeat ping = " + repeatPings.getText().toString());
                        if (TextUtils.isEmpty(repeatPings.getText().toString())) {
                            myIntent.putExtra("count", 1);
                            Log.v(LOG_TAG, "Sending Repeat ping = 1 ");
                        } else {
                            Log.v(LOG_TAG, "Sending Repeat ping = " + Integer.parseInt(repeatPings.getText().toString()));
                            myIntent.putExtra("count", Integer.parseInt(repeatPings.getText().toString()));
                        }

                        StringBuilder sb = new StringBuilder();
                        for (PingerItem s : items) {
                            sb.append(s.getHostname());
                            sb.append(",");
                        }

                        myIntent.putExtra("hostList", sb.toString()); //Optional parameters
                        startActivity(myIntent);
                        return;
                    }else{
                        Toast.makeText(PingMax.this, "No Internet connection", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PingMax.this, "No URL given", Toast.LENGTH_SHORT).show();
                }
            }
        });


        loadItems();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    private boolean AddHostName(String hostname) {
        if(!TextUtils.isEmpty(hostname.trim())) {
            PingerItem pi = new PingerItem(hostname);
            items.add(0, pi);
            pingItemsAdapter.notifyDataSetChanged();
//
//        Thread t = new Thread(new NameResolver(hostname));
//        t.start();

            if (items.size() >= MAXHOSTS) {
                myEditText.setText("Max number of host");
                myEditText.setEnabled(false);
            } else {
                myEditText.setText("");
            }
        }else{
            Toast.makeText(PingMax.this, "Empty URL will not work", Toast.LENGTH_SHORT).show();
        }

        return true;
    }

    private void loadItems() {
        // Read host from file
        try {
            InputStream in = openFileInput(SAVEFILE);

            if(in!=null) {
                InputStreamReader tmp=new InputStreamReader(in);
                BufferedReader reader=new BufferedReader(tmp);
                String hostname;
//    			StringBuffer buf=new StringBuffer();
                while ((hostname=reader.readLine()) != null) {
//    				Log.v("multiping","read hostname="+hostname);
                    AddHostName(hostname);
                }
                in.close();
                pingItemsAdapter.notifyDataSetChanged();
                myEditText.setText("");
//    			Toast.makeText(this, "saved file loaded", Toast.LENGTH_SHORT).show();
            }
        }
        catch (Throwable t)
        {
            //
        }
    }

    private void saveItems() {
        try {
            OutputStreamWriter out=
                    new OutputStreamWriter(openFileOutput(SAVEFILE, 0));
            for(int i=items.size()-1; i>=0; i--) {
                out.write(items.get(i).hostname + "\n");
            }
            out.close();
        }
        catch (Throwable t) {
            Toast.makeText(this, "Exception: " + t.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    public void onStart() {
        super.onStart();
//
//        m_background = new Thread(new Runnable() {
//            public void run() {
//                while(isRunning)
//                {
//                    for(int i=0; i<items.size(); i++) {
//                        PingerItem pi = items.get(i);
//                        if(pi.ipAddress!=null) {
//    						pi.result_80 = -1;
//    						pi.result_av = -1;
//    						items.set(i,pi);
//                            Thread t1 = new Thread(new Pinger80(pi.ia));
//                            t1.setName("Pinger80 " + pi.hostname);
//                            t1.start();
//                            Thread t2 = new Thread(new PingerAv(pi.ia));
//                            t2.setName("PingerAv " + pi.hostname);
//                            t2.start();
//                        }
//                    }
//
//                    try {
//                        for(int to=0; to<PERIOD; to+=UPDATE) {
//                            Thread.sleep(UPDATE);
//                            handler.sendMessage(handler.obtainMessage());
//                        }
//                    } catch (InterruptedException e) {
//                        Log.v("multiping","InterruptedException");
//                        break;
//                    }
//                } // end of while
//            }
//        });

        isRunning=true;
//        m_background.start();
    }

    public void onStop() {
        super.onStop();
        //saveItems();
        isRunning= false;
    }

    public void onPause() {
        super.onPause();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.quick, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bugreport:
                String versionName = "";
                try {
                    versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"talalbutt@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "BugReport " +  getString(R.string.app_name) + " " + versionName );
                startActivity( Intent.createChooser(i, "Select Email App"));
                return true;
            case R.id.refresh:
                refresh();
                return true;
        }
        return false;
    }

    private void refresh() {
        try {
            OutputStreamWriter out=
                    new OutputStreamWriter(openFileOutput(SAVEFILE, 0));
            for(int i=items.size()-1; i>=0; i--) {
                out.write(items.get(i).hostname + "\n");
            }
            out.close();
        }
        catch (Throwable t) {
            Toast.makeText(this, "Exception: " + t.toString(), Toast.LENGTH_SHORT).show();
        }

        // Restart
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    public void onBackPressed()
    {
        finish();
        return;
    }


//    public class NameResolver implements Runnable {
//        private String hostname;
//        public NameResolver(String hostname) {
//            this.hostname = hostname;
//        }
//
//        public void run() {
//            Log.v(LOG_TAG,"NameResolver for "+hostname);
//            InetAddress ia;
//            try {
//                Log.v(LOG_TAG,"ip ");
//                ia = InetAddress.getByName(hostname);
//                Log.v(LOG_TAG,"ip "+ia.toString());
//                for(int i=0; i<items.size(); i++)
//                {
//                    PingerItem pi = items.get(i);
//                    if(pi.getHostname().equals(hostname)) {
//                        Log.v(LOG_TAG,"NameResolver "+hostname + " resolved:" + ia);
//                        pi.setIpAddress(ia);
//                        items.set(i,pi);
//                    }
//                }
//            } catch (UnknownHostException e) {
//                Log.v(LOG_TAG, "Exception:" + e.getStackTrace());
//            }
//
//            Log.v(LOG_TAG,"ip ");
//        }
//    }

    public String getLocalIpAddress() {
        String sLocalIpAddress="";
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String sIpAddress = inetAddress.getHostAddress().toString();
                        if(sIpAddress.startsWith("fe80:")) {
                            // Ignore IPv6 Link local address
                        } else if(sIpAddress.startsWith("::127.") || sIpAddress.startsWith("::172.")) {
                            // Ignore local loopback address
                        } else {
                            sLocalIpAddress = sLocalIpAddress + " " + sIpAddress;

//                            Log.e(LOG_TAG, sLocalIpAddress.toString());
                        }
                    }
                }
            }
            return sLocalIpAddress;
        } catch (SocketException ex) {
            Log.e(LOG_TAG, ex.toString());
        }
        return null;
    }

}
