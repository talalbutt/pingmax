package com.network.pingmax;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by talal on 01/02/2015.
 */
class PingItemAdapter extends ArrayAdapter<PingerItem> {
    Activity context;
    ArrayList<PingerItem> items = new ArrayList<PingerItem>();

    PingItemAdapter(Activity context,ArrayList<PingerItem> items ) {
        super(context, R.layout.pingitem, items);
        this.items = items;
        this.context=context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        ViewWrapper wrapper=null;

        if(row==null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row=inflater.inflate(R.layout.pingitem, null);
            wrapper=new ViewWrapper(row);
            row.setTag(wrapper);
        }
        else {
            wrapper=(ViewWrapper)row.getTag();
        }

        int textcolor;
        String textresult;
        PingerItem pi = items.get(position);

//        long result;
//        if(pi.result_80>pi.result_av)
//            result=pi.result_av;
//        else
//            result=pi.result_80;
//
//        if(result>=100000) {
//            textcolor = Color.GRAY;
//            textresult = "wait..";
//        } else if (result>=100000) {
//            textcolor = Color.RED;
//            textresult = "timeout";
//        } else {
//            textcolor = Color.WHITE;
//            textresult = result + "ms";
//        }
//
//        String sIp;
//        if(pi.ipAddress==null)
//            sIp = "0.0.0.0";
//        else
//            sIp = pi.ipAddress.toString().replaceFirst(".*/", "");
//        wrapper.getViewHostIp().setTextColor(textcolor);
        wrapper.getViewHostIp().setText(pi.hostname + "\n" + "");
//        wrapper.getViewDelay().setTextColor(textcolor);
        wrapper.getViewDelay().setText("");
        return row;
    }
}


class ViewWrapper {
    View base;
    TextView hostip=null;
    TextView delay=null;

    ViewWrapper(View base) {
        this.base = base;
    }

    TextView getViewHostIp() {
        if (hostip==null) {
            hostip=(TextView)base.findViewById(R.id.hostip);
        }
        return hostip;
    }

    TextView getViewDelay() {
        if (delay==null) {
            delay=(TextView)base.findViewById(R.id.delay);
        }
        return delay;
    }
}