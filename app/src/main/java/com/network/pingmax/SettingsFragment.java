package com.network.pingmax;

import com.network.pingmax.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

public class SettingsFragment extends PreferenceFragment implements
		OnSharedPreferenceChangeListener {
	private static final String TAG = SettingsFragment.class.getSimpleName();
	private SharedPreferences prefs;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "Creating settings Fragment");
		addPreferencesFromResource(R.xml.settings);
	}

	@Override
	public void onStart() {
		super.onStart();
		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		prefs.registerOnSharedPreferenceChangeListener(this);
		Log.d(TAG, "Onstart settings Fragment");
	}

	@Override
	public void onStop() {
		super.onStop();
		prefs.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		getActivity().sendBroadcast( new Intent("com.kingston.welcome.patienthub.action.UPDATED_INTERVAL") );
	}
}
