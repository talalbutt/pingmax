package com.network.pingmax;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.network.pingmax.ping.PingCollection;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by talal on 01/02/2015.
 */
public class PingCollectionReportActivity extends Activity {
    private static final String LOG_TAG = PingCollectionReportActivity.class.getSimpleName();
    TextView report;
    String hostItems;
    String[] hostList;
    int count = 1;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report);
        report = (TextView) findViewById(R.id.report_long);

        Intent intent = getIntent();
        this.count = intent.getIntExtra("count", count);
        if (count > 10) {
            count = 10;
            Toast.makeText(this, "Max repeat pings = 10", Toast.LENGTH_LONG).show();
        }

        Log.v(LOG_TAG, "Received repeat ping = " + this.count);
        hostItems = intent.getStringExtra("hostList");
        String[] hostList = hostItems.split(",");
        Log.v(LOG_TAG, "Pinging " + hostList.length + " hosts:");

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String logReport = intent.getStringExtra(PingCollectionService.LOG_REPORT_LONG);
                        report.setText(logReport);
                    }
                }, new IntentFilter(PingCollectionService.ACTION_LOG_REPORT_BROADCAST)
        );

        Intent serviceIntent = new Intent(this, PingCollectionService.class);
        serviceIntent.putExtra("count", this.count);
        serviceIntent.putExtra("hostList", hostItems);
        startService(serviceIntent);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onStop();
        stopService(new Intent(this, PingCollectionService.class));
    }


    private void startPingThread(final String[] hostList) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            private long startTime = System.currentTimeMillis();

            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        report.setText("" + ((System.currentTimeMillis() - startTime) / 1000));
                        Log.d(LOG_TAG, "Work for " + hostList.length);
                        StringBuilder sReport = new StringBuilder();
                        sReport.append("Pinging " + hostList.length + " hosts: " + count + " times each\n");
                        for (String s : hostList) {
                            sReport.append(s + "\n");
                            Log.v(LOG_TAG, "Host:" + s);
                        }

                        report.setText(sReport.toString());
                        sReport.setLength(0);
                        PingCollection pingsToURLs = new PingCollection(hostList, count);
                        sReport.append("Collection Pinging " + hostList.length + " hosts: " + count + " times each\n");
                        for (String s : pingsToURLs.getHostlist()) {
                            sReport.append(s + "\n");
                            Log.v(LOG_TAG, "Host:" + s);
                        }
                        report.setText(sReport.toString());

                        pingsToURLs.startPingComparison();
                        report.setText(pingsToURLs.getLogReport());
                    }
                });
            }

        };
        new Thread(runnable).start();
    }

}
